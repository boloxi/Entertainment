var Global = require("Global");
cc.Class({
    extends: cc.Component,

    properties: {
        skipNode: [cc.Node]
    },


    onLoad:function(){

    },


    update: function (dt) {
        
        this.updateSkipUI();
    },

    updateSkipUI: function () {
        for (let i = 0; i < this.skipNode.length; i++) {
            this.skipNode[i].x -= 1;
            if (this.skipNode[i].x == -495) {
                this.skipNode[i].x = 450;
            }
        }
    },

    onSkipClick: function (sender, event) {
        var num = parseInt(event);
        var skipID = Global.skipContents[num];
        wx.navigateToMiniProgram({
            appId: skipID
        })
    }
});
