

var GameData = require('GameData');
var Global = require('Global');
cc.Class({
    extends: cc.Component,

    properties: {
        closeBtnNode: cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    onLoad() {
        var self = this;
        this.scheduleOnce(function () {
            self.closeBtnNode.active = true;
        }, 1);
    },

    setCb(cb) {
        if (cb) {
            this.cb = cb;
        }
    },

    onGetClick: function () {
        var self = this;
        var cb = function () {
            GameData.addDiamond(30);
            if (self.cb) {
                self.cb();
            }
        }
        Global.getByAD(cb);

    },

    onCloseBtn: function () {
        if (this.cb) {
            this.cb();
        }
        this.node.destroy();

    }

});
