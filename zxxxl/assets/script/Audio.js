var Global = require('Global')
cc.Class({
    extends: cc.Component,

    properties: {
        audioClips: { type: [cc.AudioClip], default: [] },
        bgMusic: { type: cc.AudioClip, default: null },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.game.addPersistRootNode(this.node);
        Global.playAudio = this;
    },


    click() {
        cc.audioEngine.play(this.audioClips[0], false, 1);
    },

    playBgMusic() {
        this.bgid = cc.audioEngine.play(this.bgMusic, true, 0.8);
    },

    stopBgMusic() {
        cc.audioEngine.stop(this.bgid);
    },

    playGood: function () {
        cc.audioEngine.play(this.audioClips[1], false, 1);
    },


    playSuccess: function () {
        cc.audioEngine.play(this.audioClips[2], false, 1);
    },

    playFail: function () {
        cc.audioEngine.play(this.audioClips[4], false, 1);
    },

    playGreat: function () {
        cc.audioEngine.play(this.audioClips[5], false, 1);
    },

    playExcellent: function () {
        cc.audioEngine.play(this.audioClips[6], false, 1);
    }

});
