
var GameData = require('GameData');
var Global = require('Global');
var REWARDNUM = 30;
cc.Class({
    extends: cc.Component,

    properties: {
        closeBtnNode: cc.Node
    },


    onGetClick: function () {
        var self = this;
        var cb = function () {
            GameData.addDiamond(REWARDNUM * 30);
            self.node.destroy();
            Global.title.setDimaondNum();
        }
        Global.getByAD(cb);
    },

    onNormalClick: function () {
        GameData.addDiamond(REWARDNUM);
        this.node.destroy();
        Global.title.setDimaondNum();
    },

    onCloseBtn: function () {
        Global.title.setDimaondNum();
        this.node.destroy();
    }

});
