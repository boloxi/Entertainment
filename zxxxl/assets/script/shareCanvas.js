cc.Class({
    extends: cc.Component,

    properties: {
    },

    onEnable:function(){
        this.tex = new cc.Texture2D();
        this.display = this.getComponent(cc.Sprite)
    },

    _updateSubDomainCanvas () {
        if (!this.tex) {
            return;
        }
        if(typeof wx == 'undefined'){
            return;
        }
        var openDataContext = wx.getOpenDataContext();
        var sharedCanvas = openDataContext.canvas;

        this.tex.initWithElement(sharedCanvas);
        this.tex.handleLoadedTexture();
        this.display.spriteFrame = new cc.SpriteFrame(this.tex);
    },

    update:function(){
        if(this.tex)
        {
            this._updateSubDomainCanvas();
        }
    },

    subMaxScore:function(score){
        if(typeof wx != 'undefined'){
            wx.postMessage({
                messageType: 2,
                MAIN_MENU_NUM: "x1",
                score:score
            })
        }
    },

    friendRank:function(){
        if(typeof wx != 'undefined'){
            wx.postMessage({
                messageType: 3,
                MAIN_MENU_NUM: "x1",
            })
        }
    },


    showNextUI:function(){
         if(typeof wx != "undefined"){
            wx.postMessage({
                messageType:5,
                MAIN_MENU_NUM: "x1",
            })
         }
    }
});
