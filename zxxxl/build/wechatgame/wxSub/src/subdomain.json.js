module.exports = [
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "ranking_number2",
      "texture": "c60zelMFBPM6P9GlMfSwZV",
      "rect": [
        16,
        12,
        38,
        35
      ],
      "offset": [
        -1.5,
        0.5
      ],
      "originalSize": [
        73,
        60
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  [
    {
      "__type__": "cc.SceneAsset",
      "_name": "Test",
      "scene": {
        "__id__": 1
      },
      "asyncLoadAssets": null
    },
    {
      "__type__": "cc.Scene",
      "_name": "New Node",
      "_children": [
        {
          "__id__": 2
        }
      ],
      "_active": false,
      "_anchorPoint": {
        "__type__": "cc.Vec2"
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 0.7668749999999999,
        "y": 0.7668749999999999,
        "z": 1
      },
      "autoReleaseAssets": false
    },
    {
      "__type__": "cc.Node",
      "_name": "Canvas",
      "_parent": {
        "__id__": 1
      },
      "_children": [
        {
          "__id__": 3
        },
        {
          "__id__": 4
        },
        {
          "__id__": 7
        },
        {
          "__id__": 25
        }
      ],
      "_level": 1,
      "_components": [
        {
          "__type__": "cc.Canvas",
          "node": {
            "__id__": 2
          },
          "_designResolution": {
            "__type__": "cc.Size",
            "width": 540,
            "height": 600
          },
          "_fitWidth": true
        },
        {
          "__type__": "e53e6YiUd9FJLf8yIQu1n47",
          "node": {
            "__id__": 2
          },
          "rankItem": {
            "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
          },
          "rankingScrollView": {
            "__id__": 7
          },
          "rankContentNode": {
            "__id__": 9
          },
          "nearRankItem": {
            "__uuid__": "f45pW4BPZI4Lh2FqKqhPkf"
          },
          "nearScrollView": {
            "__id__": 4
          },
          "nearContentNode": {
            "__id__": 6
          },
          "nextNode": {
            "__id__": 25
          },
          "nextSprite": {
            "__id__": 27
          }
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": 600
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": 270,
        "y": 300
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      },
      "_id": "1f51fRKdx9KAZIFK6yiet28"
    },
    {
      "__type__": "cc.Node",
      "_name": "Main Camera",
      "_parent": {
        "__id__": 2
      },
      "_level": 1,
      "_components": [
        {
          "__type__": "cc.Camera",
          "node": {
            "__id__": 3
          },
          "_clearFlags": 7,
          "_backgroundColor": {
            "__type__": "cc.Color",
            "a": 0
          },
          "_depth": -1
        }
      ],
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "NearRankDisplay",
      "_parent": {
        "__id__": 2
      },
      "_children": [
        {
          "__id__": 5
        }
      ],
      "_level": 2,
      "_components": [
        {
          "__type__": "cc.ScrollView",
          "node": {
            "__id__": 4
          },
          "vertical": false,
          "brake": 0.75,
          "bounceDuration": 0.23,
          "_N$content": {
            "__id__": 6
          },
          "content": {
            "__id__": 6
          },
          "_N$horizontalScrollBar": null,
          "_N$verticalScrollBar": null
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": 600
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "view",
      "_parent": {
        "__id__": 4
      },
      "_children": [
        {
          "__id__": 6
        }
      ],
      "_components": [
        {
          "__type__": "cc.Mask",
          "node": {
            "__id__": 5
          }
        },
        {
          "__type__": "cc.Widget",
          "node": {
            "__id__": 5
          },
          "_alignFlags": 45,
          "_top": 640,
          "_originalWidth": 240,
          "_originalHeight": 250
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": -40
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -320
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "content",
      "_parent": {
        "__id__": 5
      },
      "_components": [
        {
          "__type__": "cc.Widget",
          "node": {
            "__id__": 6
          },
          "_alignFlags": 45,
          "_left": 310,
          "_right": 310,
          "_originalWidth": 220,
          "_originalHeight": 400
        },
        {
          "__type__": "cc.Layout",
          "node": {
            "__id__": 6
          },
          "_layoutSize": {
            "__type__": "cc.Size",
            "width": -80,
            "height": -40
          },
          "_resize": 1,
          "_N$layoutType": 1,
          "_N$spacingX": 20
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": -80,
        "height": -40
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "RankDisplay",
      "_parent": {
        "__id__": 2
      },
      "_children": [
        {
          "__id__": 8
        }
      ],
      "_level": 2,
      "_components": [
        {
          "__type__": "cc.ScrollView",
          "node": {
            "__id__": 7
          },
          "horizontal": false,
          "brake": 0.75,
          "bounceDuration": 0.23,
          "_N$content": {
            "__id__": 9
          },
          "content": {
            "__id__": 9
          },
          "_N$horizontalScrollBar": null,
          "_N$verticalScrollBar": null
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": 600
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "view",
      "_parent": {
        "__id__": 7
      },
      "_children": [
        {
          "__id__": 9
        }
      ],
      "_components": [
        {
          "__type__": "cc.Mask",
          "node": {
            "__id__": 8
          }
        },
        {
          "__type__": "cc.Widget",
          "node": {
            "__id__": 8
          },
          "_alignFlags": 45,
          "_originalWidth": 240,
          "_originalHeight": 250
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": 600
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "content",
      "_parent": {
        "__id__": 8
      },
      "_children": [
        {
          "__id__": 10
        }
      ],
      "_components": [
        {
          "__type__": "cc.Widget",
          "node": {
            "__id__": 9
          },
          "_alignFlags": 41,
          "_originalWidth": 131.33
        },
        {
          "__type__": "cc.Layout",
          "node": {
            "__id__": 9
          },
          "_layoutSize": {
            "__type__": "cc.Size",
            "width": 540,
            "height": 100
          },
          "_resize": 1,
          "_N$layoutType": 2,
          "_N$spacingY": 20
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": 100
      },
      "_anchorPoint": {
        "__type__": "cc.Vec2",
        "x": 0.5,
        "y": 1
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": 300
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "rankItem",
      "_parent": {
        "__id__": 9
      },
      "_children": [
        {
          "__id__": 11
        },
        {
          "__id__": 12
        },
        {
          "__id__": 16
        },
        {
          "__id__": 18
        },
        {
          "__id__": 20
        },
        {
          "__id__": 22
        }
      ],
      "_level": 1,
      "_components": [
        {
          "__type__": "30480d05idFP4JTf3pksGAH",
          "node": {
            "__id__": 10
          },
          "rankLabel": {
            "__id__": 17
          },
          "scoreLabel": {
            "__id__": 21
          },
          "nickLabel": {
            "__id__": 19
          },
          "headSprite": {
            "__id__": 24
          },
          "bgNode": {
            "__id__": 11
          },
          "specialTag": {
            "__id__": 12
          },
          "selfBgSpriteFrame": {
            "__uuid__": "d8Z3OVZcdK7KZBd1w6lVPW"
          },
          "numNode": {
            "__id__": 16
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "cb1xt1i89AtImCnngy1sCf"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 460,
        "height": 100
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -50
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "ranking_person_bg",
      "_parent": {
        "__id__": 10
      },
      "_level": 2,
      "_components": [
        {
          "__type__": "cc.Sprite",
          "node": {
            "__id__": 11
          },
          "_spriteFrame": {
            "__uuid__": "d7U+Od0SJF878R+NL6BOx4"
          },
          "_sizeMode": 0
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "5fwk1FTLpPYpZkHterelX+"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": 85
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "specialTag",
      "_parent": {
        "__id__": 10
      },
      "_children": [
        {
          "__id__": 13
        },
        {
          "__id__": 14
        }
      ],
      "_active": false,
      "_level": 2,
      "_components": [
        {
          "__type__": "c308eahQF5OKYoOclS5S2o7",
          "node": {
            "__id__": 12
          },
          "crownFrames": [
            {
              "__uuid__": "29W91i7PVEVItFco4CpbcW"
            },
            {
              "__uuid__": "00WWSKuUdOppzYSCsgu4hh"
            },
            {
              "__uuid__": "d9qmW0yahAHLM/UP64xVRa"
            }
          ],
          "crownSprite": {
            "__id__": 15
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "5dnNVflhZBGYf5ry/E1nrk"
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -267
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "bg",
      "_parent": {
        "__id__": 12
      },
      "_active": false,
      "_level": 3,
      "_components": [
        {
          "__type__": "cc.Sprite",
          "node": {
            "__id__": 13
          },
          "_spriteFrame": {
            "__uuid__": "2dlqcUaNNERbW5P4W7KDxW"
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "719ExNk9NEhLEWoBIuxS+A"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 120,
        "height": 119
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -99
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 0.8,
        "y": 0.8,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "crown",
      "_parent": {
        "__id__": 12
      },
      "_level": 3,
      "_components": [
        {
          "__id__": 15
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "4423UayuBHA7LykyAOh9Ft"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 41,
        "height": 39
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": 52.8,
        "y": 0.2
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Sprite",
      "node": {
        "__id__": 14
      },
      "_spriteFrame": {
        "__uuid__": "29W91i7PVEVItFco4CpbcW"
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "num",
      "_parent": {
        "__id__": 10
      },
      "_active": false,
      "_level": 2,
      "_components": [
        {
          "__id__": 17
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "98KrOGtqpFXLL5waYwdDnt"
      },
      "_color": {
        "__type__": "cc.Color",
        "r": 255
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 33.37,
        "height": 70
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -216
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 16
      },
      "_useOriginalSize": false,
      "_string": "1",
      "_N$string": "1",
      "_fontSize": 60,
      "_lineHeight": 70,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1
    },
    {
      "__type__": "cc.Node",
      "_name": "name",
      "_parent": {
        "__id__": 10
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 19
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "20P9E02tlLB4f/gmtgfiZ0"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 197.93,
        "height": 60
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": 44
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 18
      },
      "_useOriginalSize": false,
      "_string": "player9999",
      "_N$string": "player9999",
      "_lineHeight": 60,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1,
      "_N$overflow": 2
    },
    {
      "__type__": "cc.Node",
      "_name": "score",
      "_parent": {
        "__id__": 10
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 21
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "a6YU2pziJLp7CXBqYmpKK3"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 80,
        "height": 60
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": 216
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 20
      },
      "_useOriginalSize": false,
      "_string": "90",
      "_N$string": "90",
      "_lineHeight": 60,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1,
      "_N$overflow": 2
    },
    {
      "__type__": "cc.Node",
      "_name": "head",
      "_parent": {
        "__id__": 10
      },
      "_children": [
        {
          "__id__": 23
        }
      ],
      "_level": 2,
      "_components": [
        {
          "__type__": "cc.Sprite",
          "node": {
            "__id__": 22
          },
          "_spriteFrame": {
            "__uuid__": "c9JufRhZlCS4SCWUZC+Un0"
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "92kAwRHG1GXZhIh5mTVG/7"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 125,
        "height": 116
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -124
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 0.6,
        "y": 0.6,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "icon",
      "_parent": {
        "__id__": 22
      },
      "_level": 3,
      "_components": [
        {
          "__id__": 24
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 10
        },
        "asset": {
          "__uuid__": "c2we9ZxJVLB5nOMU2nzMfZ"
        },
        "fileId": "ecQnYT26dKzqaYcVgbji/K"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 90,
        "height": 90
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1.1111111111111112,
        "y": 1.1111111111111112,
        "z": 1
      }
    },
    {
      "__type__": "cc.Sprite",
      "node": {
        "__id__": 23
      },
      "_spriteFrame": {
        "__uuid__": "8c20Sso/ZEn7NUfNSM+EBh"
      },
      "_sizeMode": 0
    },
    {
      "__type__": "cc.Node",
      "_name": "rank",
      "_parent": {
        "__id__": 2
      },
      "_children": [
        {
          "__id__": 26
        },
        {
          "__id__": 28
        },
        {
          "__id__": 29
        },
        {
          "__id__": 30
        }
      ],
      "_active": false,
      "_level": 2,
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 300,
        "height": 300
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -27
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1.5,
        "y": 1.5,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "icon",
      "_parent": {
        "__id__": 25
      },
      "_level": 3,
      "_components": [
        {
          "__id__": 27
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 90,
        "height": 90
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": 38
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1.1111111,
        "y": 1.1111111,
        "z": 1
      }
    },
    {
      "__type__": "cc.Sprite",
      "node": {
        "__id__": 26
      },
      "_spriteFrame": {
        "__uuid__": "8c20Sso/ZEn7NUfNSM+EBh"
      },
      "_sizeMode": 0
    },
    {
      "__type__": "cc.Node",
      "_name": "name",
      "_parent": {
        "__id__": 25
      },
      "_level": 3,
      "_components": [
        {
          "__type__": "cc.Label",
          "node": {
            "__id__": 28
          },
          "_string": "夏聪",
          "_N$string": "夏聪",
          "_lineHeight": 50,
          "_N$horizontalAlign": 1,
          "_N$verticalAlign": 1
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 80,
        "height": 50
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -47
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "nextLabel",
      "_parent": {
        "__id__": 25
      },
      "_level": 3,
      "_components": [
        {
          "__type__": "cc.Label",
          "node": {
            "__id__": 29
          },
          "_string": "即将超越",
          "_N$string": "即将超越",
          "_lineHeight": 50,
          "_N$horizontalAlign": 1,
          "_N$verticalAlign": 1
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 280,
        "height": 50
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": 112
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "score",
      "_parent": {
        "__id__": 25
      },
      "_level": 3,
      "_components": [
        {
          "__type__": "cc.Label",
          "node": {
            "__id__": 30
          },
          "_string": "66",
          "_N$string": "66",
          "_N$horizontalAlign": 1,
          "_N$verticalAlign": 1
        }
      ],
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 44.49,
        "height": 40
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -104
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    }
  ],
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "ranking_number1",
      "texture": "4aY0YqmNZOA5dCRmUn3GLD",
      "rect": [
        16,
        12,
        38,
        37
      ],
      "offset": [
        -1.5,
        2.5
      ],
      "originalSize": [
        73,
        66
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "banana_light",
      "texture": "8151VXA1RE3YeyYi9E1RYb",
      "rect": [
        0,
        0,
        120,
        119
      ],
      "offset": [
        0,
        0
      ],
      "originalSize": [
        120,
        119
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "default_sprite",
      "texture": "6eBWFz0oVHPLIGQKf/9Thu",
      "rect": [
        0,
        2,
        40,
        36
      ],
      "offset": [
        0,
        0
      ],
      "originalSize": [
        40,
        40
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "pm_tx",
      "texture": "c70TN91w9BiawRa9k3k9nX",
      "rect": [
        0,
        0,
        108,
        108
      ],
      "offset": [
        0,
        0
      ],
      "originalSize": [
        108,
        108
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  [
    {
      "__type__": "cc.Prefab",
      "_name": "rankItem",
      "data": {
        "__id__": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "rankItem",
      "_children": [
        {
          "__id__": 2
        },
        {
          "__id__": 3
        },
        {
          "__id__": 7
        },
        {
          "__id__": 9
        },
        {
          "__id__": 11
        },
        {
          "__id__": 13
        }
      ],
      "_level": 1,
      "_components": [
        {
          "__type__": "30480d05idFP4JTf3pksGAH",
          "node": {
            "__id__": 1
          },
          "rankLabel": {
            "__id__": 8
          },
          "scoreLabel": {
            "__id__": 12
          },
          "nickLabel": {
            "__id__": 10
          },
          "headSprite": {
            "__id__": 15
          },
          "bgNode": {
            "__id__": 2
          },
          "specialTag": {
            "__id__": 3
          },
          "selfBgSpriteFrame": {
            "__uuid__": "d8Z3OVZcdK7KZBd1w6lVPW"
          },
          "numNode": {
            "__id__": 7
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "cb1xt1i89AtImCnngy1sCf"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 460,
        "height": 100
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -50
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "ranking_person_bg",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__type__": "cc.Sprite",
          "node": {
            "__id__": 2
          },
          "_spriteFrame": {
            "__uuid__": "d7U+Od0SJF878R+NL6BOx4"
          },
          "_sizeMode": 0
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "5fwk1FTLpPYpZkHterelX+"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 540,
        "height": 85
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "specialTag",
      "_parent": {
        "__id__": 1
      },
      "_children": [
        {
          "__id__": 4
        },
        {
          "__id__": 5
        }
      ],
      "_active": false,
      "_level": 2,
      "_components": [
        {
          "__type__": "c308eahQF5OKYoOclS5S2o7",
          "node": {
            "__id__": 3
          },
          "crownFrames": [
            {
              "__uuid__": "29W91i7PVEVItFco4CpbcW"
            },
            {
              "__uuid__": "00WWSKuUdOppzYSCsgu4hh"
            },
            {
              "__uuid__": "d9qmW0yahAHLM/UP64xVRa"
            }
          ],
          "crownSprite": {
            "__id__": 6
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "5dnNVflhZBGYf5ry/E1nrk"
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -267
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "bg",
      "_parent": {
        "__id__": 3
      },
      "_active": false,
      "_level": 3,
      "_components": [
        {
          "__type__": "cc.Sprite",
          "node": {
            "__id__": 4
          },
          "_spriteFrame": {
            "__uuid__": "2dlqcUaNNERbW5P4W7KDxW"
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "719ExNk9NEhLEWoBIuxS+A"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 120,
        "height": 119
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -99
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 0.8,
        "y": 0.8,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "crown",
      "_parent": {
        "__id__": 3
      },
      "_level": 3,
      "_components": [
        {
          "__id__": 6
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "4423UayuBHA7LykyAOh9Ft"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 41,
        "height": 39
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": 52.8,
        "y": 0.2
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Sprite",
      "node": {
        "__id__": 5
      },
      "_spriteFrame": {
        "__uuid__": "29W91i7PVEVItFco4CpbcW"
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "num",
      "_parent": {
        "__id__": 1
      },
      "_active": false,
      "_level": 2,
      "_components": [
        {
          "__id__": 8
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "98KrOGtqpFXLL5waYwdDnt"
      },
      "_color": {
        "__type__": "cc.Color",
        "r": 255
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 33.37,
        "height": 70
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -216
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 7
      },
      "_useOriginalSize": false,
      "_string": "1",
      "_N$string": "1",
      "_fontSize": 60,
      "_lineHeight": 70,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1
    },
    {
      "__type__": "cc.Node",
      "_name": "name",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 10
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "20P9E02tlLB4f/gmtgfiZ0"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 197.93,
        "height": 60
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": 44
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 9
      },
      "_useOriginalSize": false,
      "_string": "player9999",
      "_N$string": "player9999",
      "_lineHeight": 60,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1,
      "_N$overflow": 2
    },
    {
      "__type__": "cc.Node",
      "_name": "score",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 12
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "a6YU2pziJLp7CXBqYmpKK3"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 80,
        "height": 60
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": 216
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      }
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 11
      },
      "_useOriginalSize": false,
      "_string": "90",
      "_N$string": "90",
      "_lineHeight": 60,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1,
      "_N$overflow": 2
    },
    {
      "__type__": "cc.Node",
      "_name": "head",
      "_parent": {
        "__id__": 1
      },
      "_children": [
        {
          "__id__": 14
        }
      ],
      "_level": 2,
      "_components": [
        {
          "__type__": "cc.Sprite",
          "node": {
            "__id__": 13
          },
          "_spriteFrame": {
            "__uuid__": "c9JufRhZlCS4SCWUZC+Un0"
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "92kAwRHG1GXZhIh5mTVG/7"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 125,
        "height": 116
      },
      "_position": {
        "__type__": "cc.Vec3",
        "x": -124
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 0.6,
        "y": 0.6,
        "z": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "icon",
      "_parent": {
        "__id__": 13
      },
      "_level": 3,
      "_components": [
        {
          "__id__": 15
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__id__": 0
        },
        "fileId": "ecQnYT26dKzqaYcVgbji/K"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 90,
        "height": 90
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1.1111111111111112,
        "y": 1.1111111111111112,
        "z": 1
      }
    },
    {
      "__type__": "cc.Sprite",
      "node": {
        "__id__": 14
      },
      "_spriteFrame": {
        "__uuid__": "8c20Sso/ZEn7NUfNSM+EBh"
      },
      "_sizeMode": 0
    }
  ],
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "headIcon",
      "texture": "d9A2nV7/FHHrXNN8yg9tly",
      "rect": [
        0,
        0,
        125,
        116
      ],
      "offset": [
        0,
        0.5
      ],
      "originalSize": [
        125,
        117
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "pht",
      "texture": "c64S8ZygRLKq+kb3wkF10P",
      "rect": [
        11,
        15,
        458,
        85
      ],
      "offset": [
        -5,
        -2
      ],
      "originalSize": [
        490,
        111
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "ranking_person1",
      "texture": "2eOUbsn+FO2KHn7b0Uj0pN",
      "rect": [
        0,
        0,
        622,
        118
      ],
      "offset": [
        0,
        0
      ],
      "originalSize": [
        622,
        118
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  {
    "__type__": "cc.Texture2D",
    "content": "0,9729,9729,33071,33071,0"
  },
  {
    "__type__": "cc.SpriteFrame",
    "content": {
      "name": "ranking_number3",
      "texture": "13cMFi7qBCHLfzh+VM5sHu",
      "rect": [
        13,
        12,
        37,
        37
      ],
      "offset": [
        -2,
        1
      ],
      "originalSize": [
        67,
        63
      ],
      "capInsets": [
        0,
        0,
        0,
        0
      ]
    }
  },
  [
    {
      "__type__": "cc.Prefab",
      "_name": "nearRankItem",
      "data": {
        "__id__": 1
      }
    },
    {
      "__type__": "cc.Node",
      "_name": "nearRankItem",
      "_children": [
        {
          "__id__": 2
        },
        {
          "__id__": 4
        },
        {
          "__id__": 6
        },
        {
          "__id__": 8
        },
        {
          "__id__": 10
        }
      ],
      "_level": 1,
      "_components": [
        {
          "__type__": "30480d05idFP4JTf3pksGAH",
          "node": {
            "__id__": 1
          },
          "rankLabel": {
            "__id__": 3
          },
          "scoreLabel": {
            "__id__": 7
          },
          "nickLabel": {
            "__id__": 5
          },
          "headSprite": {
            "__id__": 9
          }
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__uuid__": "f45pW4BPZI4Lh2FqKqhPkf"
        },
        "fileId": "bfBqtEWhVEC48IIFqQ+zAh"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 190,
        "height": 220
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      },
      "_zIndex": 0
    },
    {
      "__type__": "cc.Node",
      "_name": "num",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 3
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__uuid__": "f45pW4BPZI4Lh2FqKqhPkf"
        },
        "fileId": "55BQecQ5tAuY67RA9zNTyv"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 22.25,
        "height": 50
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": 94
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      },
      "_zIndex": 0
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 2
      },
      "_useOriginalSize": false,
      "_string": "1",
      "_N$string": "1",
      "_lineHeight": 50,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1
    },
    {
      "__type__": "cc.Node",
      "_name": "name",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 5
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__uuid__": "f45pW4BPZI4Lh2FqKqhPkf"
        },
        "fileId": "3dU0vBgjlHPqEB5IO30eLz"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 160,
        "height": 60
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -50
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      },
      "_zIndex": 0
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 4
      },
      "_useOriginalSize": false,
      "_string": "player9999",
      "_N$string": "player9999",
      "_fontSize": 30,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1,
      "_N$overflow": 2
    },
    {
      "__type__": "cc.Node",
      "_name": "score",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 7
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__uuid__": "f45pW4BPZI4Lh2FqKqhPkf"
        },
        "fileId": "55O2jcYfpB1rdKqrT0+vpn"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 160,
        "height": 40
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": -90
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      },
      "_zIndex": 0
    },
    {
      "__type__": "cc.Label",
      "node": {
        "__id__": 6
      },
      "_useOriginalSize": false,
      "_string": "99999",
      "_N$string": "99999",
      "_fontSize": 30,
      "_N$horizontalAlign": 1,
      "_N$verticalAlign": 1,
      "_N$overflow": 2
    },
    {
      "__type__": "cc.Node",
      "_name": "icon",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__id__": 9
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__uuid__": "f45pW4BPZI4Lh2FqKqhPkf"
        },
        "fileId": "b3A4lg5blKbJlBkoUKP8MX"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 80,
        "height": 80
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": 28
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      },
      "_zIndex": 0
    },
    {
      "__type__": "cc.Sprite",
      "node": {
        "__id__": 8
      },
      "_spriteFrame": {
        "__uuid__": "8c20Sso/ZEn7NUfNSM+EBh"
      },
      "_sizeMode": 0
    },
    {
      "__type__": "cc.Node",
      "_name": "pm_tx",
      "_parent": {
        "__id__": 1
      },
      "_level": 2,
      "_components": [
        {
          "__type__": "cc.Sprite",
          "node": {
            "__id__": 10
          },
          "_spriteFrame": {
            "__uuid__": "b0HhLJt6RH/7GrMea4WbQm"
          },
          "_sizeMode": 0
        }
      ],
      "_prefab": {
        "__type__": "cc.PrefabInfo",
        "root": {
          "__id__": 1
        },
        "asset": {
          "__uuid__": "f45pW4BPZI4Lh2FqKqhPkf"
        },
        "fileId": "f4TpKRtuBGvo2qdi5khiyy"
      },
      "_contentSize": {
        "__type__": "cc.Size",
        "width": 85,
        "height": 85
      },
      "_position": {
        "__type__": "cc.Vec3",
        "y": 28
      },
      "_scale": {
        "__type__": "cc.Vec3",
        "x": 1,
        "y": 1,
        "z": 1
      },
      "_zIndex": 0
    }
  ]
];
