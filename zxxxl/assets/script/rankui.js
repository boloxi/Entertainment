
var GameData = require('GameData');
cc.Class({
    extends: cc.Component,

    properties: {
        shareCanvasNode: cc.Node
    },


    onLoad: function () {
        this.shareCanvasNode.getComponent("shareCanvas").subMaxScore(GameData.maxScore);
        this.shareCanvasNode.getComponent("shareCanvas").friendRank();
    },

    onCloseClick: function () {
        this.node.active = false;
    }
});
