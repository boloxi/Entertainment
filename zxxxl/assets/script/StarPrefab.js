
const STANDWIDTH = 75
const HALFWIDTH = 5
cc.Class({
    extends: cc.Component,

    properties: {
        skins: [cc.SpriteFrame],
    },

    // use this for initialization
    onLoad: function () {
        this.node.setScale(1.17);
    },
    // 设置方块类型
    SetType: function (skinType) {
        this.skinType = skinType;
        this.getComponent(cc.Sprite).spriteFrame = this.skins[skinType - 1];
    },
    // 设置网格坐标
    SetGridPOS: function (x, y) {
        this.gridX = x;
        this.gridY = y;
        this.node.setPosition((x - HALFWIDTH) * STANDWIDTH + STANDWIDTH / 2, (y - HALFWIDTH) * STANDWIDTH + STANDWIDTH / 2);
    },


    setSelected: function (selected) {
        console.log("setSelected");
        if (!selected) {
            this.node.stopAllActions();
            this.node.setScale(1.17);
        } else {
            this.node.runAction(
                cc.repeatForever(cc.sequence(
                    cc.scaleTo(0.3, 1),
                    cc.scaleTo(0.3, 1.17)
                )));
        }
    },
    // 移动到一个网格坐标
    MoveTo: function (x, y, delay) {
        this.gridX = x;
        this.gridY = y;
        this.node.runAction(cc.sequence(
            cc.delayTime(delay),
            cc.moveTo(0.1, cc.v2((x - HALFWIDTH) * STANDWIDTH + STANDWIDTH / 2, (y - HALFWIDTH) * STANDWIDTH + STANDWIDTH / 2))
        ));
    },

});
