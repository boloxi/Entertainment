
var GameData = require('GameData');
var Global = require('Global');
cc.Class({
    extends: cc.Component,

    properties: {
        chargeNode: cc.Node,
        chargeUINode: cc.Node
    },

    onRewardClick: function () {
        this.chargeNode.active = false;
        this.chargeUINode.active = true;
        var randNum = GameData.randArr[Math.floor(Math.random() * 9)];
        GameData.addCharge(randNum);
        this.chargeUINode.getChildByName("curLabel").getComponent(cc.Label).string = GameData.telCharge + "元";
        this.chargeUINode.getChildByName('getLabel').getComponent(cc.Label).string = randNum + "元";
    },

    setCb(cb) {
        this.cb = cb
    },

    onCloseClick: function () {
        this.node.active = false;
        if (this.cb) {
            this.cb();
        }
    },

    onShareClick: function () {
        if (typeof wx != "undefined") {
            var shareContens = Global.getImageInfo();
            wx.shareAppMessage({
                title: shareContens.shareTxt,
                imageUrl: shareContens.imageUrl,
            });
        }
    }

});
