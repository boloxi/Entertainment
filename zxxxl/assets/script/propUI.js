
var Global = require("Global");
var GameData = require('GameData');
cc.Class({
    extends: cc.Component,

    properties: {
        hammerLabel: cc.Label,
        freshLabel: cc.Label,
        penLabel: cc.Label
    },

    onLoad: function () {
        this.hammerLabel.string = GameData.hammerNum;
        this.freshLabel.string = GameData.freshItemNum;
        this.penLabel.string = GameData.penItemNum;
    },

    onGetClick: function (sender, event) {

        switch (event) {
            case "0":
                Global.game.onHammerClick();
                break;
            case "1":
                Global.game.onFreshClick();
                break;
            case "2":
                Global.game.onPenBtnClick();
                break;
        }
        var self = this;
        this.scheduleOnce(function () {
            self.onCloseClick();
        })

    },

    onCloseClick: function () {
        this.node.destroy();
    }
});
