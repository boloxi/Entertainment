
var Global = require("Global");
var GameData = require('GameData');
cc.Class({
    extends: cc.Component,

    properties: {
        successNode: cc.Node,
        failNode: cc.Node,
        closeBtn: cc.Node
    },


    init(isSuccess) {
        this.isSuccess = isSuccess;
        var self = this;
        if (isSuccess) {
            Global.playAudio.playSuccess();
            this.successNode.active = true;
            this.scheduleOnce(function () {
                self.node.destroy();
            }, 1)
        }
        else {
            this.scheduleOnce(function () {
                self.closeBtn.active = true
            }, 1)
            Global.playAudio.playFail();
            this.failNode.active = true;
        }
    },


    onCloseClick: function () {
        if (this.isSuccess) {
            Global.game.Continue();
            this.node.destroy();
        }
        else {
            Global.game.onCloseClick();
            this.node.destroy();
        }
    },

    restartClick: function () {
        var self = this;
        Global.game.Continue(function () {
            self.node.destroy();
        });
    }




});
