
module.exports = {
    // 网格大小
    gridSize: 10,
    canShare: false,
    isShowBanner: true,
    isShareSuccess: false,
    noAd: false,
    isShowCharge: false,
    needToshow: false,
    isOnline: false,
    extraScore: [2000, 1820, 1560, 1320, 980, 720, 560, 480, 320, 120],
    skipContents: ["wx58ddad26d93085e6", "wxc0e46fb9869249f3", "wx67673581c69eb965", "wxebcd7899e7e36113", "wxc73270c86796b980", "wxbb8e4fae4aa2b1f6", "wx36a410a69ffad162"],

    shareContents: {
        "isShow": true,
        "prop": [
            1
        ],
        "sharePic": [
            "https://boloxi-1259173350.cos.ap-chengdu.myqcloud.com/007proj/share1.png"
        ],
        "shareTex": [
            "红包雨你敢挑战吗？"
        ]
    },

    GetScores: function (count) {
        return count * count * 5;
    },


    GetAddingScores: function (count) {
        return Math.max(0, 2000 - count * count * 20)
    },


    GetTargetScore: function (curLevel) {
        if (!this.levelCfg) { this.levelCfg = {} }
        if (this.levelCfg[curLevel.toString()]) { return this.levelCfg[curLevel.toString()] }
        var r = 1000;
        if (curLevel > 1) {
            r = this.GetTargetScore(curLevel - 1);
            r = r + (0 == r % 2 ? 2000 : 3000);
            r = r + Math.floor(curLevel / 10) * 300;
        } else if (curLevel == 0) {
            r = 0;
        }
        this.levelCfg[curLevel.toString()] = r;
        return r;
    },

    share: function (successCb) {
        if (cc.sys.isBrowser || this.noAd || !this.canShare) {
            successCb();
            return;
        }

        this.shareTime = new Date().getTime() / 1000;
        if (successCb) {
            this.shareSuccess = successCb;
        }
        var shareContens = this.getImageInfo();
        wx.shareAppMessage({
            title: shareContens.shareTxt,
            imageUrl: shareContens.imageUrl,
        });
    },

    getImageInfo: function () {
        var shareContens = {};
        var objLength = Object.keys(this.shareContents.prop).length;
        var randNum = Math.random();
        for (var i = 0; i < objLength; i++) {
            if (this.shareContents.prop[i] > randNum) {
                shareContens.imageUrl = this.shareContents.sharePic[i];
                shareContens.shareTxt = this.shareContents.shareTex[i];
                break;
            }
        }
        if (Object.keys(shareContens).length == 0) {
            shareContens = {
                imageUrl: "https://boloxi-1259173350.cos.ap-chengdu.myqcloud.com/007proj/share1.png",
                shareTxt: "红包雨你敢挑战吗？"
            }
        }
        return shareContens;
    },

    showShareTipUI: function () {
        cc.loader.loadRes('prefab/shareTip', cc.Prefab, function (err, prefab) {
            var tipNode = cc.instantiate(prefab);
            tipNode.parent = cc.find("Canvas");
            tipNode.zIndex = 999;
        })
    },

    showRewardAD(successCb, failCb, cancelCb) {
        var adId = "adunit-e4fac27fb974dff1";
        if (this.noAd) {
            if (successCb) {
                successCb();
            }
            return;
        }
        if (typeof wx == 'undefined') {
            if (failCb) {
                failCb();
            }
            successCb = null;
            failCb = null;
            cancelCb = null;
            this.videoAd = null;
            return;
        }
        cc.audioEngine.pauseAll();
        var self = this
        this.videoAd = wx.createRewardedVideoAd({
            adUnitId: adId
        })
        this.videoAd.load().then(
            () => self.videoAd.show().then(
                () => {
                    console.log('videoAd show')
                }
            )
        ).catch(
            err => console.log(err.errMsg)
        )

        this.videoAd.onLoad(() => {
            console.log('videoAd.onLoad')
        })

        this.videoAd.onError((res) => {
            if (failCb) {
                console.log('videoAd.onError:' + JSON.stringify(res))
                failCb();
            }
            successCb = null;
            failCb = null;
            cancelCb = null;
            self.videoAd = null;
        })


        this.videoAd.onClose((res) => {
            cc.audioEngine.resumeAll();
            if (res.isEnded) {
                if (successCb) {
                    console.log('videoAd.onClose show done')
                    successCb();
                }
                successCb = null;
                failCb = null;
                cancelCb = null;
                self.videoAd = null;
            } else {
                if (cancelCb) {
                    console.log('videoAd.onClose user cancelled')
                    cancelCb();
                }
                successCb = null;
                failCb = null;
                cancelCb = null;
                self.videoAd = null;
            }
        })
    },

    hideBannerAD() {
        if (this.bannerAd != null) {
            this.bannerAd.destroy()
            this.bannerAd = null
        }
    },

    showBannerAD(successCb) {
        console.log('showBannerAD')
        //每次刷新
        var visibleSize = cc.view.getVisibleSize();
        if (this.bannerAd != null) {
            this.bannerAd.destroy()
            this.bannerAd = null
        }
        if (typeof wx != 'undefined') {
            if (this.bannerAd == null) {
                console.log(this.WXSystemInfo);
                if (this.WXSystemInfo) {
                    this.bannerAd = wx.createBannerAd({
                        adUnitId: 'adunit-d7e50a3f46072a80',
                        style: {
                            left: 0,
                            top: 0,
                            width: this.WXSystemInfo.screenWidth,
                        }
                    })
                    if (this.bannerAd) {
                        var self = this;
                        this.bannerAd.onResize(function (res) {
                            self.bannerAd.style.left = self.frameSize.width / 2 - res.width / 2;
                            self.bannerAd.style.top = self.frameSize.height - res.height;
                            self.bannerAdHeight = res.height * (visibleSize.height / self.frameSize.height);
                            if (successCb) {
                                successCb();
                            }
                        });
                        this.bannerAd.onError(err => {
                            console.log(err)
                        })
                        console.log('bannerAd show')
                        this.bannerAd.show()
                    }

                }
            } else {
                console.log('bannerAd show')
                this.bannerAd.show()
            }
        }
    },


    getByAD: function (cb) {
        var self = this;
        if (!this.isShareSuccess) {
            this.share(cb);
        }
        else {
            var randNum = Math.random();
            if (randNum < 0.5) {
                this.showRewardAD(cb, function () {
                    self.share(cb);
                })
            }
            else {
                this.share(cb);
            }
        }
    }

};




