
cc.Class({
    extends: cc.Component,

    properties: {
        crownFrames:[cc.SpriteFrame],
        crownSprite:cc.Sprite,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    init(index)
    {
        this.crownSprite.spriteFrame = this.crownFrames[index];
    }

    // update (dt) {},
});
