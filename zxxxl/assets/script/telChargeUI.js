
var GameData = require('GameData');
var Global = require('Global');
cc.Class({
    extends: cc.Component,

    properties: {
        curHourLabel: cc.Label,
        curChargeLabel: cc.Label,
        chargeNode: cc.Node
    },

    onLoad: function () {
        this.lockUI = false;
        var curHour = new Date().getHours();
        this.curHourLabel.string = curHour;
        this.freshCurLabel();
    },

    freshCurLabel: function () {
        this.curChargeLabel.string = GameData.telCharge;
    },

    setCb(cb) {
        this.cb = cb;
    },

    onGetDimaondClick: function () {
        if (this.lockUI) {
            return;
        }
        this.lockUI = true;
        var self = this;
        this.scheduleOnce(function () {
            self.lockUI = false;
        }, 0.5)
        if (GameData.checkChargeEnough(1)) {
            GameData.costCharge(1);
            GameData.addDiamond(100);
            this.freshCurLabel();
            if (this.cb) {
                this.cb();
            }
        }
    },

    onCloseClick() {
        this.node.destroy();
    },

    onGetChargeClick: function () {
        this.chargeNode.active = true;
    },

    onChargeClose: function () {
        this.chargeNode.active = false;
    }


});
