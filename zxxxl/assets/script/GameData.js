module.exports = {
    //储存数据
    loginToday: 0,
    diamondNum: 0,
    maxScore: 0,
    freshItemNum: 0,
    penItemNum: 0,
    hammerNum: 0,
    telCharge: 0,
    todayRewardNum: 0,
    isGetToday: false,
    curData: {},
    isLoadComplete: false,
    isGetCollect: false,


    //临时数据

    randArr: [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2],

    initData: function (cb) {
        if (this.isLoadComplete) {
            cb();
            return;
        }

        this.loginToday = this.getDataByKey('loginToday', 0);
        this.nowDay = this.getDataByKey("nowDay", new Date().getDate());
        this.diamondNum = this.getDataByKey('diamondNum', 0);
        this.maxScore = this.getDataByKey('maxScore', 0);
        this.freshItemNum = this.getDataByKey('freshItemNum', 2);
        this.penItemNum = this.getDataByKey('penItemNum', 2);
        this.hammerNum = this.getDataByKey('hammerNum', 2);
        this.telCharge = this.getDataByKey('telCharge', 0);
        this.todayRewardNum = this.getDataByKey("todayRewardNum", 0);
        this.isGetToday = this.getDataByKey('isGetToday', 0);
        this.curData = this.getObjByKey('curData', {});
        this.isGetCollect = this.getDataByKey('isGetCollect', false)

        //判断是否是同一天
        var todayDate = new Date().getDate();
        if (this.nowDay != todayDate) {
            this.nowDay = todayDate;
            this.loginToday = 0;
            this.todayRewardNum = 0;
            this.isGetToday = 0;
            this.setDataByKey("isGetToday", 0);
            this.setDataByKey("todayRewardNum", 0);
            this.setDataByKey("loginToday", 0);
            this.setDataByKey("nowDay", this.nowDay);
        }

        if (cb) {
            this.isLoadComplete = true;
            cb();
        }
    },

    getDataByKey(key, defVal) {
        var retVal = defVal;
        if (typeof wx != 'undefined') {
            if (wx.getStorageSync(key) !== '') {
                retVal = wx.getStorageSync(key);
            }
        }
        return retVal;
    },

    getObjByKey: function (key, defVal) {
        var retVal = defVal;
        if (typeof wx != 'undefined') {
            if (wx.getStorageSync(key) !== '') {
                retVal = wx.getStorageSync(key);
            }
        }
        if (cc.sys.isBrowser) {
            if (cc.sys.localStorage.getItem(key)) {
                retVal = JSON.parse(cc.sys.localStorage.getItem(key));
            }
        }
        return retVal;
    },

    setDataByKey(key, val) {
        if (typeof wx != 'undefined') {
            wx.setStorageSync(key, val);
        }
    },

    setObjByKey: function (key, val) {
        if (typeof wx != 'undefined') {
            wx.setStorageSync(key, val);
        }
        if (cc.sys.isBrowser) {
            cc.sys.localStorage.setItem('curData', JSON.stringify(this.curData))
        }
    },

    setLoginComplete: function () {
        this.loginToday = 1;
        this.setDataByKey('loginToday', this.loginToday);
    },

    addDiamond: function (num) {
        this.diamondNum += num;
        this.setDataByKey('diamondNum', this.diamondNum);
    },

    checkDiamondEnough(cost) {
        if (this.diamondNum >= cost) {
            return true;
        }
        return false;
    },

    checkChargeEnough(cost) {
        if (this.telCharge >= cost) {
            return true;
        }
        return false;
    },

    costCharge(cost) {
        if (!this.checkChargeEnough(cost)) {
            return;
        }
        this.telCharge = parseFloat((this.telCharge - cost).toFixed(1));
        this.setDataByKey('telCharge', this.telCharge);
    },

    addCharge(num) {
        this.telCharge = parseFloat((this.telCharge + num).toFixed(1));
        this.todayRewardNum += num;
        if (this.telCharge >= 18) {
            this.telCharge = 18;
        }
        this.setDataByKey('telCharge', this.telCharge);
        this.setDataByKey('todayRewardNum', this.todayRewardNum);
    },

    costDimaond(cost) {
        if (!this.checkDiamondEnough(cost)) {
            return;
        }
        this.diamondNum -= cost;
        this.setDataByKey('diamondNum', this.diamondNum);
    },

    saveMaxScore: function (score) {
        this.maxScore = score;
        this.setDataByKey("maxScore", this.maxScore);
    },

    saveFreshNum: function () {
        this.setDataByKey("freshItemNum", this.freshItemNum);
    },

    savePenNum: function () {
        this.setDataByKey("penItemNum", this.penItemNum);
    },

    saveHammerNum: function () {
        this.setDataByKey("hammerNum", this.hammerNum);
    },

    saveIsGetToday: function () {
        this.setDataByKey('isGetToday', this.isGetToday);
    },

    saveCurData: function (data) {
        this.curData = data;
        this.setObjByKey('curData', this.curData);
    },

    saveIsGetCollect: function () {
        this.isGetCollect = true;
        this.setDataByKey('isGetCollect', this.isGetCollect);
    }
}
