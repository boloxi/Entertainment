
var Global = require('Global');
var GameData = require('GameData');
var Net = require('Net');
cc.Class({
    extends: cc.Component,

    properties: {

        loginTipPrefab: cc.Prefab,
        telNode: cc.Node,
        diamonTipdPrefab: cc.Prefab,
        diamondLabel: cc.Label,
        telChargeLabel: cc.Label,
        telChargePrefab: cc.Prefab,
        rankNode: cc.Node,
        maxLabel: cc.Label,
        hfNode: cc.Node,
        contiueNode: cc.Node,
        addNode: cc.Node,
        topSkipSprite: [cc.Sprite],
        collcetNode: cc.Node,
        diamondLabelNode: cc.Node,
        xdSprite: cc.Sprite
    },

    onLoad: function () {
        var self = this;
        GameData.initData(function () {
            Global.wxInstant.initWX(function () {
                self.init();
            });
        })
        Global.playAudio.playBgMusic();
        Global.title = this;
        this.showIcon();
        if (Global.isOnline) {
            this.showDiamond();
        }
        this.xdIcon();

    },

    xdIcon: function () {
        var self = this;
        if (typeof window.xhtad != "undefined") {
            console.log("xhtad onload");
            var adtype = "fixed"
            window.xhtad.xhtAdsData(adtype).then(ads => {
                console.log('ads is ', ads)
                var adName = ads.adName
                var picUrl = ads.img;
                var skipImg = ads.qrImg

                cc.loader.load(picUrl, function (err, texture) {
                    let spriteFrame = new cc.SpriteFrame(texture);
                    self.xdSprite.spriteFrame = spriteFrame;
                    self.xdSprite.node.on(cc.Node.EventType.TOUCH_START, function () {
                        xhtad.xhtAdsClick(adName)
                        wx.previewImage({
                            urls: [skipImg]
                        })
                    })
                })
            })

        }
    },

    showDiamond: function () {
        this.diamondLabelNode.active = true;
    },

    onDestroy: function () {
        Global.title = null;
    },

    showIcon: function () {
        var self = this;
        if (typeof wx != "undefined") {
            Net.requestUrl(Net.homeSkipUrl, function (data) {
                for (let i = 0; i < data.length; i++) {
                    self.scheduleOnce(function () {
                        cc.loader.load(data[i].pic, function (err, texture) {
                            let spriteFrame = new cc.SpriteFrame(texture);
                            self.topSkipSprite[i].spriteFrame = spriteFrame;
                            self.topSkipSprite[i].node.active = true;
                            self.topSkipSprite[i].node.on(cc.Node.EventType.TOUCH_START, function () {
                                wx.previewImage({
                                    urls: [data[i].skip]
                                })
                            })
                        })

                    })
                }
            })

        }
    },

    setDimaondNum: function () {
        this.diamondLabel.string = GameData.diamondNum;
        this.showReward();
    },

    showReward: function () {
        if (GameData.todayRewardNum >= 5 || GameData.telCharge >= 18 || Global.needToshow == false) {
            return;
        }
        GameData.saveIsGetCollect();
        this.addNode.active = true;
    },

    init: function () {
        this.telChargeLabel.string = GameData.telCharge;
        this.diamondLabel.string = GameData.diamondNum;
        if (!GameData.loginToday) {
            var loginTipNode = cc.instantiate(this.loginTipPrefab);
            loginTipNode.parent = this.node;
            GameData.setLoginComplete();
        }
        else {
            this.showReward();
        }
        this.maxLabel.string = "最高分：" + GameData.maxScore;
        if (Global.isShowCharge) {
            this.hfNode.active = true;
            this.collcetNode.active = true;
        }
    },

    onStartClick: function () {
        if (JSON.stringify(GameData.curData) != "{}") {
            this.contiueNode.active = true;
        }
        else {
            cc.director.loadScene("GameScene");
            Global.playAudio.stopBgMusic();
        }
    },

    onShareClick: function () {
        if (typeof wx != "undefined") {
            var shareContens = Global.getImageInfo();
            wx.shareAppMessage({
                title: shareContens.shareTxt,
                imageUrl: shareContens.imageUrl,
            });
        }
    },



    onDiamondClick: function () {
        var self = this;
        var diamondNode = cc.instantiate(this.diamonTipdPrefab);
        diamondNode.parent = this.node;
        diamondNode.getComponent("diamondTip").setCb(function () {
            self.diamondLabel.string = GameData.diamondNum;
            diamondNode.destroy();
        })


    },

    onGetTelChargeClick: function () {
        var telChargeNode = cc.instantiate(this.telChargePrefab);
        telChargeNode.parent = this.node;
        var self = this;
        telChargeNode.getComponent("telChargeUI").setCb(function () {
            self.diamondLabel.string = GameData.diamondNum;
            self.telChargeLabel.string = GameData.telCharge;
        })
    },

    onRankClick: function () {
        this.rankNode.active = true;
    },

    onContinueClick: function (sender, msg) {
        Global.playAudio.stopBgMusic();
        if (msg == "true") {
            cc.director.loadScene('GameScene');
        }
        else if (msg == "false") {
            var data = {};
            GameData.saveCurData(data);
            cc.director.loadScene('GameScene');
        }
    }
});
