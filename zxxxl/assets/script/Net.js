module.exports = {
    gameSkipUrl: "https://boloxi-1259173350.cos.ap-chengdu.myqcloud.com/007proj/gameSkip/skip.json",
    cloudUrl: "https://boloxi-1259173350.cos.ap-chengdu.myqcloud.com/007proj/starcloud.json",
    homeSkipUrl: "https://boloxi-1259173350.cos.ap-chengdu.myqcloud.com/007proj/homeSkip/skip.json",

    requestUrl: function (url, callback, failback) {
        if (typeof wx != "undefined") {
            wx.request({
                url: url,
                success: function (res) {
                    callback(res.data);
                },
                fail: function (res) {
                    if (failback) {
                        failback();
                    }
                }
            })
        }
    }
}
