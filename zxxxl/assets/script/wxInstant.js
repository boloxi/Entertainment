
var Global = require('Global');
var GameData = require('GameData');
var Net = require('Net');
cc.Class({
    extends: cc.Component,

    properties: {

    },

    onLoad: function () {
        Global.wxInstant = this;
        cc.game.addPersistRootNode(this.node);
        this.isLoadComplete = false;
    },

    initWX(cb) {
        var self = this;
        if (typeof wx == "undefined" || this.isLoadComplete == true) {
            cb();
            return;
        }
        var LaunchOption = wx.getLaunchOptionsSync();

        if (LaunchOption.scene == 1104 && !GameData.isGetCollect) {
            Global.needToshow = true;
        }
        var curTime = new Date().getTime() / 1000;
        var url = Net.cloudUrl + "?" + curTime;
        Global.WXSystemInfo = wx.getSystemInfoSync();
        Global.frameSize = cc.view.getFrameSize();

        Global.showBannerAD();
        Net.requestUrl(url, function (data) {
            Global.canShare = data.canShare;
            Global.isShowBanner = data.isShowBanner;
            if (data.shareContents.isShow) {
                Global.shareContents = data.shareContents;
            };
            Global.isShowCharge = data.isShowCharge;
            Global.isOnline = data.isOnline;
            if (Global.isOnline && Global.title) {
                Global.title.showDiamond();
            }
            cb();
            self.isLoadComplete = true;
        }, function () {
            cb();
            self.isLoadComplete = true;
        })

        wx.onShow(res => {
            var curTime = new Date().getTime() / 1000;
            if (Global.shareTime != null && Global.showShareTipUI) {
                if (curTime - Global.shareTime > 2.5) {
                    if (Global.shareSuccess && Global.shareSuccess != null) {
                        Global.shareSuccess();
                        Global.isShareSuccess = true;
                        Global.shareSuccess = null;
                    }
                }
                else {
                    Global.showShareTipUI();
                }
            }

            if (res.scene == 1104 && !GameData.isGetCollect) {
                Global.needToshow = true;
                if (Global.title != null) {
                    Global.title.showReward();
                }

            }
        });

        wx.showShareMenu({
            withShareTicket: true,
            success: () => {
            }
        });

        wx.updateShareMenu({
            withShareTicket: true,
            success() {
            }
        })


    }
});
