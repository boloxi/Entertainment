
var Global = require("Global");
cc.Class({
    extends: cc.Component,

    properties: {
        goodNode: [cc.Node]
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        var self = this;
        this.node.runAction(cc.sequence(cc.moveBy(0.3, cc.v2(0, 20)), cc.delayTime(0.5), cc.callFunc(function () {
            self.node.destroy();
        })))
    },

    showUI: function (index) {
        if (index >= 8 && index < 15) {
            this.goodNode[0].active = true;
            Global.playAudio.playGood();
        }
        else if (index >= 15 && index < 20) {
            this.goodNode[1].active = true;
            Global.playAudio.playGreat();
        }
        else if (index >= 20) {
            this.goodNode[2].active = true;
            Global.playAudio.playExcellent();
        }
    }
});
