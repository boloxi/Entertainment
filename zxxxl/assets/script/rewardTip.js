
var Global = require("Global");
var GameData = require('GameData');
cc.Class({
    extends: cc.Component,

    properties: {
        closeBtnNode: cc.Node,
        rewardNode: [cc.Node],
        bgNode: cc.Node,
        chargeNode: cc.Node,
        chargeUINode: cc.Node,
        diamondLabel: cc.Label,
        skipNode: cc.Node
    },


    onDestroy() {
        Global.game.isGetGift = false;
    },

    onLoad: function () {
        var self = this;
        this.randNum = -1;
        this.scheduleOnce(function () {
            self.closeBtnNode.active = true;
        }, 1)

        if (GameData.todayRewardNum >= 5 || GameData.telCharge >= 18) {
            this.getReward = "gift";
        }
        else {
            if (!GameData.isGetToday) {
                this.getReward = "money";
                GameData.isGetToday = 1;
                GameData.saveIsGetToday();
            }
            else {
                var randNum = Math.random();
                if (randNum < 0.5) {
                    this.getReward = "money";
                }
                else {
                    this.getReward = "gift";
                }
            }

        }


        if (!Global.isShowCharge) {
            this.getReward = "gift";
        }


        this.showUI();
    },

    showUI: function () {
        if (this.getReward == "gift") {
            this.bgNode.active = true;
        }
        else if (this.getReward == "money") {
            this.chargeNode.active = true;
            var self = this;
            this.scheduleOnce(function () {
                self.chargeNode.getChildByName("nothanks").active = true;
            }, 1)

        }
    },

    setCb(cb) {
        this.cb = cb;
    },

    onGetGiftClick: function () {
        var self = this;
        var cb = function () {
            self.skipNode.active = true;
            self.bgNode.active = false;
            if (self.randNum == -1) {
                self.randNum = Math.ceil(Math.random() * 4) - 1;
            }

            self.rewardNode[self.randNum].active = true;
            switch (self.randNum) {
                case 0:
                    GameData.penItemNum++;
                    GameData.savePenNum();
                    break;
                case 1:
                    GameData.hammerNum++;
                    GameData.saveHammerNum();
                    break;
                case 2:
                    GameData.freshItemNum++;
                    GameData.saveFreshNum();
                    break;
                case 3:
                    var randDiamondNum = Math.floor(Math.random() * 40) + 20;
                    GameData.addDiamond(randDiamondNum);
                    self.diamondLabel.string = randDiamondNum;
                    break;
            }
            Global.game.freshUI();
        }

        Global.getByAD(cb)

    },



    ongetChargeClick: function () {
        var self = this;
        var cb = function () {
            self.chargeNode.active = false;
            self.chargeUINode.active = true;
            var randNum = GameData.randArr[Math.floor(Math.random() * 9)];
            GameData.addCharge(randNum);
            self.chargeUINode.getChildByName("curLabel").getComponent(cc.Label).string = GameData.telCharge + "元";
            self.chargeUINode.getChildByName('getLabel').getComponent(cc.Label).string = randNum + "元";
            Global.game.freshUI();
        }
        Global.getByAD(cb);
    },


    onCloseClick() {
        this.cb();
        this.node.destroy();
    },

    onShareClick: function () {
        if (typeof wx != "undefined") {
            var shareContens = Global.getImageInfo();
            wx.shareAppMessage({
                title: shareContens.shareTxt,
                imageUrl: shareContens.imageUrl,
            });
        }
    }
});
