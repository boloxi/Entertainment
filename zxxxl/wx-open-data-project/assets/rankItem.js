
cc.Class({
    extends: cc.Component,

    properties: {
       rankLabel:cc.Label,
       scoreLabel:cc.Label,
       nickLabel:cc.Label,
       headSprite:cc.Sprite,
       bgNode:cc.Node,
       specialTag:cc.Node,
       selfBgSpriteFrame:cc.SpriteFrame,
       numNode:cc.Node
    },


    init: function (rank, data, isSelf) {
        if(this.specialTag)
        {
            if(rank < 3)
            {
                console.log('show rank:' + rank)
                this.specialTag.active = true;
                this.numNode.active = false;
                this.specialTag.getComponent('specialTag').init(rank);
            }else{
                this.specialTag.active = false;
                this.numNode.active = true;
            }
        }
        if(this.bgNode)
        {
            if(isSelf)
            {
                console.log('show self')
                this.bgNode.getComponent(cc.Sprite).spriteFrame = this.selfBgSpriteFrame;
            }
        }
        console.log('rankItem data:' + JSON.stringify(data))
        let avatarUrl = data.avatarUrl;
        let nick = data.nickname.length <= 10 ? data.nickname : data.nickname.substr(0, 10) + "...";
        //let nick = data.nickname;
        let grade = data.KVDataList.length != 0 ? data.KVDataList[0].value : 0;

        this.rankLabel.string = (rank + 1).toString();
        this.createImage(avatarUrl);
        this.nickLabel.string = nick;
        this.scoreLabel.string = grade.toString();
    },

    createImage(avatarUrl) {
        if (CC_WECHATGAME) {
            try {
                let image = wx.createImage();
                image.onload = () => {
                    try {
                        let texture = new cc.Texture2D();
                        texture.initWithElement(image);
                        texture.handleLoadedTexture();
                        this.headSprite.spriteFrame = new cc.SpriteFrame(texture);
                    } catch (e) {
                        cc.log(e);
                        this.headSprite.node.active = false;
                    }
                };
                image.src = avatarUrl;
            }catch (e) {
                cc.log(e);
                this.headSprite.node.active = false;
            }
        } else {
            cc.loader.load({
                url: avatarUrl, type: 'jpg'
            }, (err, texture) => {
                this.headSprite.spriteFrame = new cc.SpriteFrame(texture);
            });
        }
    }

});
